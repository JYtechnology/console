#ifndef __INCLUDE_CONSOLE_HEADER_H__
#define __INCLUDE_CONSOLE_HEADER_H__
#include "elib/lib2.h"
#include "elib/lang.h"
#include "elib/krnllib.h"

#include "console_cmd_typedef.h"

#define CONSOLE_EXTERN_C EXTERN_C

#ifndef __E_STATIC_LIB
extern LIB_CONST_INFO g_ConstInfo_console_global_var[];
extern int g_ConstInfo_console_global_var_count;
extern CMD_INFO g_cmdInfo_console_global_var[];
extern PFN_EXECUTE_CMD g_cmdInfo_console_global_var_fun[];
extern int g_cmdInfo_console_global_var_count;
extern ARG_INFO g_argumentInfo_console_global_var[];
extern LIB_DATA_TYPE_INFO g_DataType_console_global_var[];
extern int g_DataType_console_global_var_count;
#endif

#define CONSOLE_DEF_CMD(_index, _szName, _szEgName, _szExplain, _shtCategory, _wState, _dtRetValType, _wReserved, _shtUserLevel, _shtBitmapIndex, _shtBitmapCount, _nArgCount, _pBeginArgInfo) \
    EXTERN_C void CONSOLE_NAME(_index, _szEgName)(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf);
CONSOLE_DEF(CONSOLE_DEF_CMD) // 所有实现命令的声明

#endif
