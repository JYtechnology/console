#pragma once

#define __CONSOLE_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__E_FNENAME)##_##_name##_##_index##_

// 传递函数名和索引, 拼接成 定义库名_名字_序号_定义库名, 比如 console_test_0_console
#define CONSOLE_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__CONSOLE_NAME(_index, _name))__LIB2_FNE_NAME_LEFT(__E_FNENAME)

// 传递函数名和索引, 拼接成 "定义库名_名字_序号_定义库名", 比如 "console_test_0_console"
#define CONSOLE_NAME_STR(_index, _name) ______E_FNENAME(__CONSOLE_NAME(_index, _name))

// 这个宏定义了所有的命令, 以后需要命令名数组, 声明命令等, 都可以使用这个宏
#define CONSOLE_DEF(_MAKE) \
    _MAKE(  0, "构造", construct, "构造函数", -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED | CT_IS_OBJ_CONSTURCT_CMD, _SDT_NULL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_console_global_var + 0)\
    _MAKE(  1, "析构", free, "析构函数", -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED | CT_IS_OBJ_FREE_CMD, _SDT_NULL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_console_global_var + 0)\
    _MAKE(  2, "清屏", Clear, "清空屏幕", -1, _CMD_OS(__OS_WIN), _SDT_NULL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_console_global_var + 0)\
    _MAKE(  3, "输入", InPut, "从控制台接收输入的数据，如果“横坐标”或“纵坐标”为空，那么从当前光标位置开始输入，注意，输入时不过滤特殊字符，如退格键等", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 7, g_argumentInfo_console_global_var + 0)\
    _MAKE(  4, "输出", OutPut, "向控制台输出数据，如果“横坐标”或“纵坐标”为空，那么从当前光标位置开始输出", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 6, g_argumentInfo_console_global_var + 7)\
    _MAKE(  5, "置光标位置", SetCursorPosition, "设置光标位置", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_console_global_var + 13)\
    _MAKE(  6, "取光标位置", GetCursorPosition, "获得光标位置", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_console_global_var + 15)\
    _MAKE(  7, "取显示区大小", GetScreenSize, "取显示区大小", -1, _CMD_OS(__OS_WIN), _SDT_NULL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_console_global_var + 17)\
    _MAKE(  8, "显示光标", ShowCursor, "显示光标", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_console_global_var + 0)\
    _MAKE(  9, "隐藏光标", HideCursor, "隐藏光标", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_console_global_var + 0)\
    _MAKE( 10, "填充背景颜色", FillBackColor, "填充背景颜色，返回实际填充的长度，失败返回-1", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 4, g_argumentInfo_console_global_var + 19)\
    _MAKE( 11, "填充字符", FillChar, "填充字符", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 4, g_argumentInfo_console_global_var + 23)

