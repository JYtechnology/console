#include "include_console_header.h"

// 本命令被隐藏, 原始名字 = "构造", 本命令为构造函数
// 调用格式: _SDT_NULL (控制台对象).构造, 命令说明: "构造函数"
// 无参数
CONSOLE_EXTERN_C void console_construct_0_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "析构", 本命令为析构函数
// 调用格式: _SDT_NULL (控制台对象).析构, 命令说明: "析构函数"
// 无参数
CONSOLE_EXTERN_C void console_free_1_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: _SDT_NULL (控制台对象).清屏, 命令说明: "清空屏幕"
// 无参数
CONSOLE_EXTERN_C void console_Clear_2_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (控制台对象).输入, 命令说明: "从控制台接收输入的数据，如果“横坐标”或“纵坐标”为空，那么从当前光标位置开始输入，注意，输入时不过滤特殊字符，如退格键等"
// 参数<1>: [横坐标 SDT_INT], 参数说明: "指定输入数据时光标的横坐标"
// 参数<2>: [纵坐标 SDT_INT], 参数说明: "指定输入数据时光标的纵坐标"
// 参数<3>: 保存当前光标 SDT_BOOL, 参数说明: "是否保存当前光标位置"
// 参数<4>: 是否回显 SDT_BOOL, 参数说明: "是否把输入的数据回显到控制台设备"
// 参数<5>: [回显数据 SDT_TEXT], 参数说明: "回显的数据，如果忽略，则使用输入的字符，在“是否回显”为真时有意义"
// 参数<6>: 回车结束 SDT_BOOL, 参数说明: "是否以回车结束输入"
// 参数<7>: 最大接收长度 SDT_INT, 参数说明: "最大接收数据长度，“回车结束”为真时有意义"
CONSOLE_EXTERN_C void console_InPut_3_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;
    BOOL     arg3 = pArgInf[3].m_bool;
    BOOL     arg4 = pArgInf[4].m_bool;
    LPSTR    arg5 = pArgInf[5].m_pText;
    BOOL     arg6 = pArgInf[6].m_bool;
    INT      arg7 = pArgInf[7].m_int;

}

// 调用格式: SDT_BOOL (控制台对象).输出, 命令说明: "向控制台输出数据，如果“横坐标”或“纵坐标”为空，那么从当前光标位置开始输出"
// 参数<1>: [横坐标 SDT_INT], 参数说明: "指定输出数据时光标的横坐标"
// 参数<2>: [纵坐标 SDT_INT], 参数说明: "指定输出数据时光标的纵坐标"
// 参数<3>: 保存当前光标 SDT_BOOL, 参数说明: "是否保存当前光标位置"
// 参数<4>: 前景颜色 SDT_INT, 参数说明: "前景即字体的颜色，颜色值为“控制台颜色”数据类型中定义的值"
// 参数<5>: 背景颜色 SDT_INT, 参数说明: "背景的颜色，颜色值为“控制台颜色”数据类型中定义的值"
// 参数<6>: 输出数据 SDT_TEXT, 参数说明: "待输出的数据"
CONSOLE_EXTERN_C void console_OutPut_4_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;
    BOOL     arg3 = pArgInf[3].m_bool;
    INT      arg4 = pArgInf[4].m_int;
    INT      arg5 = pArgInf[5].m_int;
    LPSTR    arg6 = pArgInf[6].m_pText;

}

// 调用格式: SDT_BOOL (控制台对象).置光标位置, 命令说明: "设置光标位置"
// 参数<1>: 横坐标 SDT_INT, 参数说明: "光标的新横坐标"
// 参数<2>: 纵坐标 SDT_INT, 参数说明: "光标的新纵坐标"
CONSOLE_EXTERN_C void console_SetCursorPosition_5_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (控制台对象).取光标位置, 命令说明: "获得光标位置"
// 参数<1>: &横坐标 SDT_INT, 参数说明: "光标的横坐标"
// 参数<2>: &纵坐标 SDT_INT, 参数说明: "光标的新纵坐标"
CONSOLE_EXTERN_C void console_GetCursorPosition_6_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;

}

// 调用格式: _SDT_NULL (控制台对象).取显示区大小, 命令说明: "取显示区大小"
// 参数<1>: &宽度 SDT_INT, 参数说明: "显示区宽度，以字符为单位"
// 参数<2>: &高度 SDT_INT, 参数说明: "显示区高度，以字符为单位"
CONSOLE_EXTERN_C void console_GetScreenSize_7_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;

}

// 调用格式: SDT_BOOL (控制台对象).显示光标, 命令说明: "显示光标"
// 无参数
CONSOLE_EXTERN_C void console_ShowCursor_8_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (控制台对象).隐藏光标, 命令说明: "隐藏光标"
// 无参数
CONSOLE_EXTERN_C void console_HideCursor_9_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (控制台对象).填充背景颜色, 命令说明: "填充背景颜色，返回实际填充的长度，失败返回-1"
// 参数<1>: 横坐标 SDT_INT, 参数说明: "开始填充的横坐标"
// 参数<2>: 纵坐标 SDT_INT, 参数说明: "开始填充的纵坐标"
// 参数<3>: 背景颜色 SDT_INT, 参数说明: "背景颜色"
// 参数<4>: 填充长度 SDT_INT, 参数说明: "填充区域的长度"
CONSOLE_EXTERN_C void console_FillBackColor_10_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;
    INT      arg3 = pArgInf[3].m_int;
    INT      arg4 = pArgInf[4].m_int;

}

// 调用格式: SDT_INT (控制台对象).填充字符, 命令说明: "填充字符"
// 参数<1>: 横坐标 SDT_INT, 参数说明: "开始填充的横坐标"
// 参数<2>: 纵坐标 SDT_INT, 参数说明: "开始填充的纵坐标"
// 参数<3>: 填充字符 SDT_TEXT, 参数说明: "本参数是一个文本，但只有第一个字符有效"
// 参数<4>: 填充长度 SDT_INT, 参数说明: "填充区域的长度"
CONSOLE_EXTERN_C void console_FillChar_11_console(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;
    LPSTR    arg3 = pArgInf[3].m_pText;
    INT      arg4 = pArgInf[4].m_int;

}

